var whitelist = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*()[]{}.";
$(".password-options").each(function(){
	var t = $(this);
	t.find("input").on('change', (function(){var r=function(){
		$(t.data("target")).attr("type", this.checked?"text":"password");
	};r.call(t.find("input")[0]);return r;})());
	t.find("button").on('click', function(e){
		e.preventDefault();
		var text = "";
		for(var i=0; i<25; i++) {
			text += whitelist.charAt(Math.floor(Math.random() * whitelist.length));
		}
		$("#dbpass").val(text);
	});
});