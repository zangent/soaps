<?php
	// This is the template for the blog admin page.
	// It's in soaps_framework because it's not really meant to be changed.
	// You can, of course, easily modify this just like any other template, though.
	define('ONLY_RENDER_TEMPLATE', 1);
	include("blog.php");
	blog_page_init();
?>
<style>
<?php include("admin_css_inc.php"); ?>
</style>
<div class="blog-admin">
	
</div>
<script>

</script>