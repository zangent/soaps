<?php define('ONLY_RENDER_TEMPLATE', 1); ?>

<!DOCTYPE html>
<html>
	<head>
		<style><?php include('admin_css_inc.php'); ?></style>
	</head>
	<body class="admin-page">
		<div class="admin-page-cont">

			<?php section_header("SOAPS Blog Setup"); ?>
			<form class="admin-form" method="post" action="<?php echo BLOG_PATH; ?>/admin/api/setup">
				<?php form_elem("Create User?", "checkbox", "makeuser"); ?>
				<span id="admin-rootinfo" class="form-conditional" style="display: none;">
					<?php
						form_elem("Database Root Username", "text", "dbrootuser", "root");
						form_elem("Database Root Password", "text", "dbrootpass");
						form_elem("Create Database?", "checkbox", "makedb");
					?>
				</span>
				<?php
					form_elem("Database Host", "text", "dbhost", "127.0.0.1", "ip");
					form_elem("Database Username", "text", "dbuser", "", "username");
					form_elem("Database Password", "password", "dbpass", "", "password");
					form_elem("Database Name", "text", "dbname", "", "username"); ?>
				<h5>Note: the DB user credentials will also be used to create a temporary account on the site.<br>
					Once the blog is set up, you should probably add a different user, then delete the auto-generated one.<br>
					Having to use the DB's password to access the site is surely terrible security practice, after all.
				</h5>
				<input type="submit" value="Submit">
			</form>
			<script src="//code.jquery.com/jquery.min.js"></script>
			<script>
				var makeuser = $("#makeuser");
				makeuser.on('change', (function(){var r=function(){
					$("#admin-rootinfo, #generate-password").css("display", makeuser[0].checked?"":"none");
				};r();return r;})());
				<?php include("admin_js.js"); ?>
			</script>

		</div>
	</body>
</html>