<?php

$make_user =    $_POST['makeuser'] == "on";
$db_root_un =   $_POST['dbrootuser'];
$db_root_pass = $_POST['dbrootpass'];
$make_table =   $_POST['makedb'] == "on";
$db_host =      $_POST['dbhost'];
$db_user =      $_POST['dbuser'];
$db_pass =      $_POST['dbpass'];
$db_name =      $_POST['dbname'];

if($db_host == "" || $db_user == "" || $db_pass == "" || $db_name == "" || ($make_user && ($db_root_un == ""))) {
	die("{'err': 'Please be sure to fill out all fields that are visible.'}");
}

try{
	$pdo = new PDO("mysql:host=$db_host;charset=utf8", $db_root_un, $db_root_pass);
	$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	if($make_table) {
		// First thing's first: we have to make the table!
		$pdo->exec("CREATE DATABASE `$db_name`");
	}
	if($make_user) {
		// Now, create the user.
		$pdo->exec("CREATE USER '$db_user'@'localhost' IDENTIFIED BY '$db_pass';
		            GRANT ALL ON `$db_name`.* TO '$db_user'@'localhost';
		            FLUSH PRIVILEGES;");
	}
	$pdo->exec("CREATE TABLE `$db_name`.`users` (
		`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
		`username` VARCHAR(30) NOT NULL,
		`password` CHAR(128) NOT NULL,
		`salt` CHAR(128) NOT NULL,
		`perms` INT NOT NULL,
		`real_name` VARCHAR(48) NOT NULL);
		CREATE TABLE `$db_name`.`posts` (
		`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
		`author_id` INT NOT NULL,
		`contents` TEXT NOT NULL);
		CREATE TABLE `$db_name`.`attempts` (
		`user_id` INT NOT NULL,
		`time` VARCHAR(30) NOT NULL
		);
		");
	// TODO(zachary): Login attempt throttling.
	$pdo = null;
} catch(PDOException $e) {
	die("{'err': " . json_encode($e->getMessage()) . "}");
}

try{
	$pdo = new PDO("mysql:host=$db_host;charset=utf8;dbname=$db_name", $db_user, $db_pass, [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]);
	$pdo = null;
} catch(PDOException $e) {
	die("{'err': " . json_encode("Failed to connect to DB using given username, password, and database.\n\n" . $e->getMessage()) . "}");
}

$conn_settings = ["db" => $db_name, "un" => $db_user, "pw" => $db_pass, "hn" => $db_host];
$file_handle = fopen(dirname(__FILE__) . "/config.json", 'w') or die("{'err': 'Failed to open config.json for writing. Check PHP\'s permissions.'}");
fwrite($file_handle, json_encode($conn_settings));
fclose($file_handle);

die("{'err': false}");