<?php

// Configuration
include_once('config.php');
include_once('utils.php');
include_once('path_to_regexp.php');

// Directory where templates are located.
define('TEMPLATE_DIRECTORY', TPL_DIR . '/templates');
// Same, but for actions/api calls/etc.
define('ACTION_DIRECTORY', TPL_DIR . '/actions');

// Raw routes. These are the only part of this
// file that should really change much.
//
// The routes are written in a way that matches closely the final URL
// The only real difference is the inclusion of variables in the mix.
// Any pseudo-directory prefixed by a colon is treated as a variable, and
// is therefore passed to the template in an array called PAGE_ARGS.
//
// For example, let's pretend there's a route that looks like this:
// "/user/:userid/overview" => "user/overview.php"
//
// This would route any "/user/ID/overview" requests to
// "soaps_website/templates/user/overview.php". In addition, that template
// would have a variable it can access called "PAGE_ARGS" (no dollar sign),
// which contains a "userid" field.
//
// If someone accessed "/user/100/overview", and the template's code was
// "<?php echo PAGE_ARGS['userid'];" the resulting page would just be "100",
// wrapped inside of the header and the footer.
//
$routes = [
	"/"             => [ "home.php", ["testid" => ""] ],
	"/home"         => [ "home.php", ["testid" => "No id!"] ],
	"/home/:testid" => [ "home.php", [] ],
	"/404/:path"    => [ "404.php",  [] ]
];
// These are the same as routes. The only two differences is that the targeted
// PHP file is searched for inside of "/actions" instead of "/templates" and
// they don't return headers or footers ever. These should be used
// to map things like "/user/:id/delete", which are less of a "page" per se
// and more like an API call.
$actions = [

];

// NOTE: Some of the routes on the site may be added later.
//       For example, the blog system adds its own routes later.



// =============================================
// From here down should probably be left alone.
// =============================================

if(BLOG_ENABLED) {
	include("blog/blog.php");
}

// Routes after being turned into regexes paired with argument describing arrays.
$processed_routes = [];

// Convert links to use /index.php?path= if rewrite is disabled.
// Otherwise, maintain rewrite paths.
// Additionally, take local <a> links and make them grab the templates with
// AJAX instead of actually changing the page.
function fix_links($page) {
	return preg_replace_callback('/<\s*a.*?href.*?>/m', function($matches) {
		$atag = $matches[0];

		$link_matches = [];
		preg_match('/href=([\'"])(.*?)\1/', $atag, $link_matches);
		$link = $link_matches[2];
		$route = route($link);
		if(!is_null($route)) {

			// Fix links if rewrite is not enabled
			$fixed_link = REWRITE_ENABLED?$link:"/index.php?path=".$link;
			$atag = str_replace($link_matches[0], "href=".$link_matches[1].$fixed_link.$link_matches[1], $atag);

			// Add the "sajlink" class if the link is to something handled
			// by this router, so that the SOAPS javascript can dynamically
			// swap templates.
			if(strpos($atag, "class") !== false) {
				$atag = preg_replace('/class\s*=\s*([\'"])/m', 'class=\1sajlink ', $atag);
			} else {
				$atag = str_replace(">", " class='sajlink' >", $atag);
			}

			// Add /templates in front of the SOAPS link.
			// That way, it only downloads the page contents.
			// TODO(zachary): This may not be required.
			// $atag = str_replace(">", " data-soaps-link='/templates" . $link /* $route[0][1] */ . "' >", $atag);
		}
		return $atag;
	}, $page);
}

function render($route, $matches, $template_mode) {

	// This code takes the matches from the regular expression,
	// and turns it into an associative array based on the
	// variable names provided inside of the route (prefixed by a ":")
	$final_args = $route[1][1];
	for($i=0;$i<count($matches)-1;$i++) {
		$final_args[$route[0]["args"][$i]] = $matches[$i+1];
	}

	$PAGE_ARGS = $final_args;

	// Using output buffering to grab the page contents.
	// I'm not including it inline so that things like the page title
	// can be set inside of the template.
	ob_start();
	include(($route[2]?TEMPLATE_DIRECTORY:ACTION_DIRECTORY) . "/" . $route[1][0]);
	$page_contents = ob_get_clean();

	// Now, to output the full page.
	// Once again, we're buffering the output. The reason here
	// is so that we can go back and fix the links so that
	// they point to index.php?path= if we don't have rewrite enabled.
	//
	// Also, we're going to add the "sajlnk" (SOAPS Ajax Link) class
	// to all local links.
	if(!$template_mode && !defined('ONLY_RENDER_TEMPLATE')) {
		ob_start();
		include(TEMPLATE_DIRECTORY . "/header.php");
		echo "<script>window._SOAPS_CONFIG={rewrite:".(REWRITE_ENABLED?"1":"0")."}</script>";
		echo "<div id='soaps-body'>" . $page_contents . "</div>";
		include(TEMPLATE_DIRECTORY . "/footer.php");
		$page_contents = ob_get_clean();
	}

	// Fix the links (see fix_links), and output the page!
	print(fix_links($page_contents));
}

// Find the requested page.
function route($path) {
	global $processed_routes;

	$template_mode = false;

	// Check to see if we're returning this template-style or like a full page.
	if(substr($path, 0, 10)=='/templates') {
		$template_mode = true;
		$path = substr($path, 10);
	}


	$matches = []; // Logically, this fits better inside of the loop.
	               // That said, it would cause a whole lot of needless
	               // memory allocations. Therefore, it's out here.

	foreach ($processed_routes as $route) {

		if(preg_match($route[0]["regex"], $path, $matches)) {

			return [$route, $matches, $template_mode || (!$route[2])];

		}
	}

	return null;
}

// Processing routes for easier use.
foreach ($routes as $path => $tpl) {

	//                    [Route URL,     Page PHP, Is a full page?]
	$processed_routes[] = [parse_path($path), $tpl, true];
}

// Same for actions, now.
foreach ($actions as $path => $tpl) {

	//                    [Route URL,   Action PHP, Is a full page?]
	$processed_routes[] = [parse_path($path), $tpl, false];
}



// Create a standardized form of the pathname.
if(!isset($_GET['path'])) $path = '/';
else $path = '/'.$_GET['path'];

$requested_page = route($path);
if(is_null($requested_page)) {
	$template = "";
	if(substr($_GET['path'], 0, 10)=='templates/') {
		$template = "/templates";
	}
	$requested_page = route("$template/404/".urlencode($path));
	header("HTTP/1.0 404 Not Found");
	if(is_null($requested_page)) {
		$fourohfour = <<<PAGEEND
<!DOCTYPE html><head><meta charset="utf-8"><title>404 - File Not Found</title></head><body>
	<h1><span style='color: red;'>404</span> - File Not Found</h1>
	<p>The server could not find the page you're looking for (REPME)</p>
	<p>Additionally, the server could not find a user-friendly error page.<br>
	   That means that this is just a mistake on the site owner's part. Sorry!</p>
</body></html>
PAGEEND;
		$fourohfour = str_replace("REPME", $path, $fourohfour);
		die($fourohfour);
	}
}

// We were able to find a page, since it hasn't died yet.
// Let's go ahead and render it.
render($requested_page[0], $requested_page[1], $requested_page[2]);
