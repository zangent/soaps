// SOAPS JS MODULE
// This file has no external JS dependancies.
//
// This Javascript is code that should
// probably be included in every SOAPS project.
//
// If this file gets in your way, you probably
// shouldn't use SOAPS (although picking apart the code of SOAPS could help :D )

// TODO(zachary): Support for not using rewrite is _not_ finished!

let container = document.getElementById('soaps-body');

let event_system = {
	'page_change': []
}

// If any of our links reference "index.php?path" then rewrite is _not_ enabled,
// because soaps is the only thing that should make those links, and they only point to
// index.php if rewrite is disabled on the server.
const rewrite_enabled = document.getElementsByClassName("sajlink")[0].href.indexOf("index.php?") == -1;

export function init() {
	ajax_links(document.body);
	for(let i=0; i<event_system['page_change'].length; i++) {
		event_system['page_change'][i]();
	}
}

export function addEventListener(name, cb) {
	if(name in event_system) {
		event_system[name].push(cb);
	} else {
		throw new Execption("Event type not recognized!");
	}
}

// AJAX Links

// Loop over all links with the class 'sajlink'
// The SOAPS PHP has already gone through and mapped
// local <a> tags to a router request. That way,
// I know what links are AJAX-able, and how to request the template.
let loading_page = false;

function ajax_links(cont) {

	let links = document.getElementsByClassName('sajlink');

	for(let i = 0; i < links.length; i++) {
		let link = links[i];
		// The callback definition is weird here
		// so that I can be sure that it captures the correct
		// `link`, otherwise it will end up capturing the last
		// `link` in the list, because JS used an awkward pseudo-pointer
		// storage system for objects.
		link.addEventListener('click', ((href, title)=>{return (evt)=>{

			history.pushState({}, title, href);
			window.onpopstate(); // Manually trigger the `history navigation` event
			evt.preventDefault();
		}})(link.href, typeof link.title == 'undefined' ? "" : link.title));
	}
}

// This is called when using the browser's history controls
// to navigate between pages
window.onpopstate = (evt) => {
	load_template("/templates" + (rewrite_enabled?location.pathname:location.search.substr(6)), location.href);
}

function load_template(tpl, fallback) {

	if(loading_page) return;
	loading_page = true;

	var ajax = new XMLHttpRequest();
	ajax.onreadystatechange = function() {
		if (ajax.readyState == 4) {
			if (ajax.status == 200 || ajax.status == 404) {

				container.innerHTML = ajax.responseText;
				ajax_links(container);
				for(let i=0; i<event_system['page_change'].length; i++) {
					event_system['page_change'][i]();
				}

			} else if(typeof fallback != 'undefined') {

				window.location.href = fallback;
			} else {
				// TODO(zachary): Come up with a better error reporting system!
				alert("Error loading template (server code " + ajax.status + ")\n---------\n" + ajax.responseText);
			}
			loading_page = false;
		}
	}
	ajax.open("GET", tpl, true);
	ajax.send(null);
}