
const random_prefix = "tm_" + Math.random().toString(36).substring(2, 7) + "_";

function internal_print(thing) {
	console.log(random_prefix + thing);
}

export function mul10(int) {
	internal_print(10 * int);
}
export function sub5(int) {
	internal_print(int - 5);
}
