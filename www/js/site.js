/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.l = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };

/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};

/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};

/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 1);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
Object.defineProperty(exports,'__esModule',{value:!0});exports.init=init;exports.addEventListener=addEventListener;// SOAPS JS MODULE
// This file has no external JS dependancies.
//
// This Javascript is code that should
// probably be included in every SOAPS project.
//
// If this file gets in your way, you probably
// shouldn't use SOAPS (although picking apart the code of SOAPS could help :D )
// TODO(zachary): Support for not using rewrite is _not_ finished!
var container=document.getElementById('soaps-body'),event_system={page_change:[]},rewrite_enabled=-1==document.getElementsByClassName('sajlink')[0].href.indexOf('index.php?');// If any of our links reference "index.php?path" then rewrite is _not_ enabled,
// because soaps is the only thing that should make those links, and they only point to
// index.php if rewrite is disabled on the server.
function init(){ajax_links(document.body);for(var a=0;a<event_system.page_change.length;a++)event_system.page_change[a]()}function addEventListener(a,b){if(a in event_system)event_system[a].push(b);else throw new Execption('Event type not recognized!')}// AJAX Links
// Loop over all links with the class 'sajlink'
// The SOAPS PHP has already gone through and mapped
// local <a> tags to a router request. That way,
// I know what links are AJAX-able, and how to request the template.
var loading_page=!1;function ajax_links(){var a=document.getElementsByClassName('sajlink');for(var b=0;b<a.length;b++){var c=a[b];// The callback definition is weird here
// so that I can be sure that it captures the correct
// `link`, otherwise it will end up capturing the last
// `link` in the list, because JS used an awkward pseudo-pointer
// storage system for objects.
c.addEventListener('click',function(d,e){return function(f){history.pushState({},e,d),window.onpopstate(),f.preventDefault()}}(c.href,'undefined'==typeof c.title?'':c.title))}}// This is called when using the browser's history controls
// to navigate between pages
window.onpopstate=function(){load_template('/templates'+(rewrite_enabled?location.pathname:location.search.substr(6)),location.href)};function load_template(a,b){if(!loading_page){loading_page=!0;var c=new XMLHttpRequest;c.onreadystatechange=function(){if(4==c.readyState){if(200==c.status||404==c.status){container.innerHTML=c.responseText,ajax_links(container);for(var e=0;e<event_system.page_change.length;e++)event_system.page_change[e]()}else'undefined'==typeof b?alert('Error loading template (server code '+c.status+')\n---------\n'+c.responseText):window.location.href=b;loading_page=!1}},c.open('GET',a,!0),c.send(null)}}

/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
var _soaps_module=__webpack_require__(0),soaps_module=_interopRequireWildcard(_soaps_module);function _interopRequireWildcard(obj){if(obj&&obj.__esModule)return obj;var newObj={};if(null!=obj)for(var key in obj)Object.prototype.hasOwnProperty.call(obj,key)&&(newObj[key]=obj[key]);return newObj.default=obj,newObj}// Your code goes here.
// You might want to modularize it, and just
// load the modules here to keep things simple.
soaps_module.addEventListener("page_change",function(){var a=document.querySelector("header>nav"),b=a.getElementsByClassName("active")[0],c=a.querySelector("a[href='"+location.pathname+"']");b&&b.classList.remove("active"),c&&c.classList.add("active")}),soaps_module.init();// Load the SOAPS JS module.

/***/ })
/******/ ]);