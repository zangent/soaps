<h1>
	Error <span style="color: red">404</span>
	<small style="font-size:.5em; position: relative; top: -.25em;"> &nbsp; (File Not Found)</small>
</h1>

<p>The server could not find the page you're looking for (<?php echo urldecode($PAGE_ARGS['path']); ?>)</p>
<p>(This is the default SOAPS 404 page)</p>
<a href="/" class="btn-home-404">Go back home</a>